export class Activity {
  _id: string;
  startDate: Date;
  endDate: Date;
  description: string;
  endHour: string;
  amount: number;
  groupSize: number;
  interests: string;
  location: string;
  startHour: string;
  title: string;
  imgSrc: string;
}
