import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { UserEditComponent } from './user-edit/user-edit.component';



@NgModule({
  declarations: [
    LoginComponent,
    SignupComponent,
    UserEditComponent
  ],
  imports: [
    CommonModule
  ]
})
export class AuthModule { }
