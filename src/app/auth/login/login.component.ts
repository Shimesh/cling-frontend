import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  googleImg: string =  '../../assets/icons/google.png';

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  navToSignup(){
    this.router.navigateByUrl("/signup")
  }
}
