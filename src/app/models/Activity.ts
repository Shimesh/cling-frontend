export interface Activity {
  _id: string,
  title: string,
  description: string,
  startDate: string,
  endDate: string,
  startHour: string,
  endHour: string,
  groupSize: number,
  interests: string[],
  location: string,
  imgSrc: string,
  cost: number
}
