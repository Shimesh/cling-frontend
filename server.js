//Install express server
const express = require('express');
const path = require('path');

const app = express();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// Serve only the static files form the dist directory
app.use(express.static(__dirname + '/dist/ClingClient'));

app.get('/*', function (req, res) {

  res.sendFile(path.join(__dirname + '/dist/ClingClient/index.html'));
});

// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || 8080);